package com.example.lelanddavisradiobuttons;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.RadioGroup;
import android.os.Bundle;
import android.widget.RadioButton;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.editText);
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Log.d("RD", "Attendees Logged.");

            }
        });
        radioGroup = (RadioGroup) findViewById(R.id.myRadio);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId){
                radioButton = (RadioButton) findViewById(checkedId);

                switch (radioButton.getId()){
                    case R.id.YesId:{
                        Log.d("RD", "YES!!");
                    }
                    break;
                    case R.id.NoId:{
                        Log.d("RD", "NO!!");
                    }
                    break;
                    case R.id.MaybeId:{
                        Log.d("RD", "Maybe!!");
                    }
                    break;
                }

            }
        });
    }
}
